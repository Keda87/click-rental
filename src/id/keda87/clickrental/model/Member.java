package id.keda87.clickrental.model;

public class Member {

    private String id;
    private String nama;
    private String alamat;
    private String nomorhandphone;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNomorhandphone() {
        return nomorhandphone;
    }

    public void setNomorhandphone(String nomorhandphone) {
        this.nomorhandphone = nomorhandphone;
    }
}
