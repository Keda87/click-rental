package id.keda87.clickrental.model;

public class Nominal {

    private int hargaSewa;
    private int dendaSewa;
    private String pk;

    public int getHargaSewa() {
        return hargaSewa;
    }

    public void setHargaSewa(int hargaSewa) {
        this.hargaSewa = hargaSewa;
    }

    public int getDendaSewa() {
        return dendaSewa;
    }

    public void setDendaSewa(int dendaSewa) {
        this.dendaSewa = dendaSewa;
    }

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }
}
