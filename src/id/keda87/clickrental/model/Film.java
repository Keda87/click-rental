package id.keda87.clickrental.model;

public class Film {

    private int id;
    private String namaFilm;
    private String tahunTerbit;
    private String produksi;
    private String statusKembali;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNamaFilm() {
        return namaFilm;
    }

    public void setNamaFilm(String namaFilm) {
        this.namaFilm = namaFilm;
    }

    public String getTahunTerbit() {
        return tahunTerbit;
    }

    public void setTahunTerbit(String tahunTerbit) {
        this.tahunTerbit = tahunTerbit;
    }

    public String getProduksi() {
        return produksi;
    }

    public void setProduksi(String produksi) {
        this.produksi = produksi;
    }

    public String getStatusKembali() {
        return statusKembali;
    }

    public void setStatusKembali(String statusKembali) {
        this.statusKembali = statusKembali;
    }
}
