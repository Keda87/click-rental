package id.keda87.clickrental.controller;

import id.keda87.clickrental.dao.ConcreteFilmDao;
import id.keda87.clickrental.dao.ConcreteMemberDao;
import id.keda87.clickrental.dao.ConcreteNominalDao;
import id.keda87.clickrental.dao.ConcreteTransaksiDao;
import id.keda87.clickrental.model.Film;
import id.keda87.clickrental.model.Member;
import id.keda87.clickrental.model.Transaksi;
import id.keda87.clickrental.view.sub.SubPeminjaman;
import id.keda87.clickrental.view.sub.SubPengembalian;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.joda.time.DateTime;
import org.joda.time.Days;

public class ControllerTransaksi {

    private SubPeminjaman viewpinjam;
    private SubPengembalian viewkembali;
    private ConcreteTransaksiDao serviceTransaksi;
    private ConcreteFilmDao serviceFilm;
    private ConcreteMemberDao serviceMember;
    private ConcreteNominalDao serviceNominal;
    private List<Transaksi> daftarTransaksi;
    private List<Film> daftarNamaFilm;
    private List<Film> daftarKodeFilm;
    private List<Member> daftarIDMember;
    private List<Member> identitasMember;

    public ControllerTransaksi(SubPeminjaman viewpinjam) {
        this.viewpinjam = viewpinjam;
        serviceTransaksi = new ConcreteTransaksiDao();
        serviceFilm = new ConcreteFilmDao();
        serviceMember = new ConcreteMemberDao();
        serviceNominal = new ConcreteNominalDao();
        daftarNamaFilm = serviceFilm.getAllNamaFilm();
        daftarIDMember = serviceMember.getAllIDMember();
    }

    public ControllerTransaksi(SubPengembalian viewkembali) {
        this.viewkembali = viewkembali;
        serviceFilm = new ConcreteFilmDao();
        serviceTransaksi = new ConcreteTransaksiDao();
        serviceNominal = new ConcreteNominalDao();
        daftarTransaksi = serviceTransaksi.getAllTransaksi();
    }

    public void isiComboNamaFilm() {
//        viewpinjam.getTfnamafilm().removeAllItems();
        viewpinjam.getModelComboFilm().removeAllElements();
        daftarNamaFilm = serviceFilm.getAllNamaFilm();
//        viewpinjam.getTfnamafilm().addItem("");
        viewpinjam.getModelComboFilm().addElement("");
        for (Film namafilm : daftarNamaFilm) {
//            viewpinjam.getTfnamafilm().addItem(namafilm.getNamaFilm());
            viewpinjam.getModelComboFilm().addElement(namafilm.getNamaFilm());
        }
    }

    public void isiComboIDMember() {
//        viewpinjam.getTfidmember().removeAllItems();
        viewpinjam.getModelComboID().removeAllElements();
        daftarIDMember = serviceMember.getAllIDMember();
//        viewpinjam.getTfidmember().addItem("");
        viewpinjam.getModelComboID().addElement("");
        for (Member id : daftarIDMember) {
//            viewpinjam.getTfidmember().addItem(id.getId());
            viewpinjam.getModelComboID().addElement(id.getId());
        }
    }

    public void isiFormMember() {
        identitasMember = serviceMember.getIdentitasMember(viewpinjam.getTfidmember().getSelectedItem().toString());
        for (Member member : identitasMember) {
            viewpinjam.getTfnamapenyewa().setText(member.getNama());
            viewpinjam.getTfnohandphone().setText(member.getNomorhandphone());
            viewpinjam.getTfalamat().setText(member.getAlamat());
        }
    }

    public void isiSewa() {
        if (viewpinjam.getTfnamapenyewa().getText().isEmpty() || viewpinjam.getTfnohandphone().getText().isEmpty()
                || viewpinjam.getTfalamat().getText().isEmpty() || viewpinjam.getTfidfilm().getText().isEmpty()
                || viewpinjam.getTftanggalpinjam().getCalendar() == null || viewpinjam.getTftanggalkembali().getCalendar() == null) {
            JOptionPane.showMessageDialog(viewpinjam, "Data Harap Diisi Lengkap !", "Kesalahan", JOptionPane.WARNING_MESSAGE);
        } else {
            viewpinjam.getTftotaltransaksi().setText(ControllerStaff.formatUang((double) serviceNominal.getSewa()));
        }
    }

    public void isiIDFilm() {
        daftarKodeFilm = serviceFilm.getAllKodeFilm(viewpinjam.getTfnamafilm().getSelectedItem().toString());
        for (Film member : daftarKodeFilm) {
            viewpinjam.getTfidfilm().setText(String.valueOf(member.getId()));
        }

        if (viewpinjam.getTfnamafilm().getSelectedIndex() == 0) {
            viewpinjam.getTfidfilm().setText("");
        }
    }

    public void insert() {
        if (viewpinjam.getTfnamapenyewa().getText().isEmpty() || viewpinjam.getTfnohandphone().getText().isEmpty() || viewpinjam.getTfalamat().getText().isEmpty()
                || viewpinjam.getTfnamafilm().getSelectedIndex() == 0 || viewpinjam.getTftanggalpinjam().getCalendar() == null
                || viewpinjam.getTftanggalkembali().getCalendar() == null) {
            JOptionPane.showMessageDialog(viewpinjam, "Form Harap Diisi lengkap & Tidak Kosong.", "Kesalahan", JOptionPane.WARNING_MESSAGE);
        } else {
            Transaksi transaksi = new Transaksi();

            if (viewpinjam.getTfCekMember().isSelected()) {
                transaksi.setIdMember(viewpinjam.getTfidmember().getSelectedItem().toString());
            } else {
                transaksi.setIdMember("Non Member");
            }

            transaksi.setNama(viewpinjam.getTfnamapenyewa().getText().trim().toUpperCase());
            transaksi.setHandphone(viewpinjam.getTfnohandphone().getText().trim().toUpperCase());
            transaksi.setAlamat(viewpinjam.getTfalamat().getText().trim().toUpperCase());
            transaksi.setNamaFilm(viewpinjam.getTfnamafilm().getSelectedItem().toString());
            transaksi.setIdFilm(Integer.parseInt(viewpinjam.getTfidfilm().getText().trim().toUpperCase()));

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String pinjam = sdf.format(viewpinjam.getTftanggalpinjam().getDate());
            transaksi.setTanggalPinjam(pinjam);
            String kembali = sdf.format(viewpinjam.getTftanggalkembali().getDate());
            transaksi.setTanggalKembali(kembali);

            transaksi.setTotalTransaksi(kembalikanNilaiSemula(viewpinjam.getTftotaltransaksi().getText().trim()));
            transaksi.setTerlambat(0);
            transaksi.setDenda(0);
            try {
                serviceTransaksi.insertTransaksi(transaksi);
                String judulFilm = viewpinjam.getTfnamafilm().getSelectedItem().toString();
                serviceFilm.updateKetersediaan(judulFilm, "Tidak Tersedia");
                JOptionPane.showMessageDialog(viewpinjam, "Data Transaksi Berhasil Disimpan.", "Tambah Data Berhasil", JOptionPane.INFORMATION_MESSAGE);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(viewpinjam, "Data Transaksi Gagal Disimpan., Terjadi Kesalahan Pada MySQL.", "Kesalahan", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    private int kembalikanNilaiSemula(String formatUang) {
        String tmp = formatUang.replaceAll("\\s|[a-z]|[A-Z]|\\.|(,00)", "");
        return Integer.parseInt(tmp);
    }

    public void hitungPembayaran() {
        if (viewpinjam.getTfpembayaran().getText().isEmpty()) {
            JOptionPane.showMessageDialog(viewpinjam, "Harap Isi  Uang Pembayaran !", "Kesalahan", JOptionPane.WARNING_MESSAGE);
        } else {
            int totalTransaksi = kembalikanNilaiSemula(viewpinjam.getTftotaltransaksi().getText().trim());
            int uangBayar = kembalikanNilaiSemula(viewpinjam.getTfpembayaran().getText().trim());
            int uangKembalian = uangBayar - totalTransaksi;
            System.out.println(totalTransaksi);
            System.out.println(uangBayar);
            System.out.println(uangKembalian);
            viewpinjam.getTfkembalian().setText(ControllerStaff.formatUang((double) uangKembalian));
        }
    }

    private int hitungKeterlambatan(int row) {
        String tanggalKembali = viewkembali.getTabel_pengembalian().getValueAt(row, 8).toString();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String tanggalPengembalian = sdf.format(viewkembali.getTftanggalkembali().getDate());

        Date mulai = null;
        Date kembali = null;

        try {
            mulai = sdf.parse(tanggalKembali);
            kembali = sdf.parse(tanggalPengembalian);
        } catch (ParseException ex) {
            Logger.getLogger(ControllerTransaksi.class.getName()).log(Level.SEVERE, null, ex);
        }
        DateTime d1 = new DateTime(mulai);
        DateTime d2 = new DateTime(kembali);
        int selisih = Days.daysBetween(d1, d2).getDays();
        return selisih;
    }

    public void pengembalian(int row) {
        if (row == -1) {
            JOptionPane.showMessageDialog(viewpinjam, "Harap Pilih Data Yang Akan Dieksekusi !", "Kesalahan", JOptionPane.WARNING_MESSAGE);
        }

        Transaksi trn = new Transaksi();
        int besarDenda = hitungKeterlambatan(row) * serviceNominal.getDenda();
        String id = viewkembali.getTabel_pengembalian().getValueAt(row, 0).toString();
        String judulFilm = viewkembali.getTabel_pengembalian().getValueAt(row, 5).toString();

        trn.setTerlambat(hitungKeterlambatan(row));
        trn.setDenda(besarDenda);
        trn.setIdTransaksi(Integer.parseInt(id));

        serviceFilm.updateKetersediaan(judulFilm, "Tersedia");
        serviceTransaksi.updateTransaksi(trn);

        viewkembali.getTfketerlambatan().setText(String.valueOf(hitungKeterlambatan(row)));
        viewkembali.getTftotaldenda().setText(ControllerStaff.formatUang((double) besarDenda));

        System.out.println("Keterlambatan: " + hitungKeterlambatan(row) + " hari");
        System.out.println("Denda: " + hitungKeterlambatan(row) + "*" + serviceNominal.getDenda() + " = " + besarDenda);
    }

    public void kosongkanForm() {
        viewpinjam.getTfidmember().setSelectedIndex(0);
        viewpinjam.getTfCekMember().setSelected(false);
        viewpinjam.getTfnamapenyewa().setText("");
        viewpinjam.getTfnohandphone().setText("");
        viewpinjam.getTfalamat().setText("");
        viewpinjam.getTfnamafilm().setSelectedIndex(0);
        viewpinjam.getTfidfilm().setText("");
        viewpinjam.getTftanggalpinjam().setCalendar(null);
        viewpinjam.getTftanggalkembali().setCalendar(null);
        viewpinjam.getTftotaltransaksi().setText("");
        viewpinjam.getTfpembayaran().setText("");
    }

    public void kosongkanFormPengembalian() {
        viewkembali.getTftanggalkembali().setCalendar(null);
        viewkembali.getTfbayardenda().setText("");
        viewkembali.getTfkembalian().setText("");
        viewkembali.getTftotaldenda().setText("");
        viewkembali.getTfketerlambatan().setText("");
    }

    public void hitungPembayaranDenda() {
        int bayar = Integer.parseInt(viewkembali.getTfbayardenda().getText().trim());
        int denda = kembalikanNilaiSemula(viewkembali.getTftotaldenda().getText().trim());
        int kembalian = bayar - denda;
        viewkembali.getTfkembalian().setText(ControllerStaff.formatUang((double) kembalian));
    }

    public void isiTablePengembalian() {
        daftarTransaksi = serviceTransaksi.getAllTransaksi();
        Object[][] data = new Object[daftarTransaksi.size()][12];
        int baris = 0;
        for (Transaksi transaksi : daftarTransaksi) {
            data[baris][0] = transaksi.getIdTransaksi();
            data[baris][1] = transaksi.getIdMember();
            data[baris][2] = transaksi.getNama();
            data[baris][3] = transaksi.getHandphone();
            data[baris][4] = transaksi.getAlamat();
            data[baris][5] = transaksi.getNamaFilm();
            data[baris][6] = transaksi.getIdFilm();
            data[baris][7] = transaksi.getTanggalPinjam();
            data[baris][8] = transaksi.getTanggalKembali();
            data[baris][9] = ControllerStaff.formatUang((double) transaksi.getTotalTransaksi());
            data[baris][10] = transaksi.getTerlambat();
            data[baris][11] = ControllerStaff.formatUang((double) transaksi.getDenda());
            baris++;
        }
        String[] header = {"ID", "ID MEMBER", "NAMA", "HANDPHONE", "ALAMAT", "FILM", "ID FILM", "TANGGAL PINJAM", "TANGGAL KEMBALI", "TOTAL", "KETERLAMBATAN", "DENDA"};
        DefaultTableModel model = new DefaultTableModel(data, header);
        viewkembali.getTabel_pengembalian().setModel(model);
    }
}
