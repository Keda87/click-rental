package id.keda87.clickrental.controller;

import id.keda87.clickrental.dao.ConcreteNominalDao;
import id.keda87.clickrental.model.Nominal;
import id.keda87.clickrental.view.sub.SubSettingHarga;
import java.util.List;

public class ControllerSettingHarga {

    private SubSettingHarga sewden;
    private ConcreteNominalDao serviceNominal;
    private List<Nominal> konfigurasiSewaDenda;

    public ControllerSettingHarga(SubSettingHarga sewden) {
        this.sewden = sewden;
        serviceNominal = new ConcreteNominalDao();
        konfigurasiSewaDenda = serviceNominal.getSewaDenda();
    }
    
    public void isiKonfigurasi(){
        konfigurasiSewaDenda = serviceNominal.getSewaDenda();
        for (Nominal sewa_denda : konfigurasiSewaDenda) {
            sewden.getTfDenda().setText(String.valueOf(sewa_denda.getDendaSewa()));
            sewden.getTfSewa().setText(String.valueOf(sewa_denda.getHargaSewa()));
        }
    }
    
    public void update(){
        if(sewden.getKunci().isSelected()){
            Nominal nominal = new Nominal();
            nominal.setDendaSewa(Integer.parseInt(sewden.getTfDenda().getText()));
            nominal.setHargaSewa(Integer.parseInt(sewden.getTfSewa().getText()));
            serviceNominal.updateNominal(nominal);
        }
    }
}
