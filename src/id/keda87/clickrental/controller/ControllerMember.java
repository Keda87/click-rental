package id.keda87.clickrental.controller;

import id.keda87.clickrental.dao.ConcreteMemberDao;
import id.keda87.clickrental.model.Member;
import id.keda87.clickrental.view.sub.SubLihatMember;
import id.keda87.clickrental.view.sub.SubTambahMember;
import java.util.Iterator;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class ControllerMember {

    SubTambahMember memberview1;
    SubLihatMember memberview2;
    ConcreteMemberDao serviceMember = new ConcreteMemberDao();
    List<Member> daftarMember;

    public ControllerMember(SubTambahMember member) {
        this.memberview1 = member;
        daftarMember = serviceMember.getAllMember();
    }

    public ControllerMember(SubLihatMember member) {
        this.memberview2 = member;
        daftarMember = serviceMember.getAllMember();
    }

    public void insert() {
        if (memberview1.getTfidentitas().getText().isEmpty() || memberview1.getTfnama().getText().isEmpty()
                || memberview1.getTfhandphone().getText().isEmpty() || memberview1.getTfalamat().getText().isEmpty()) {
            JOptionPane.showMessageDialog(memberview1, "Form Harap Diisi lengkap & Tidak Kosong.", "Kesalahan", JOptionPane.WARNING_MESSAGE);
        } else {
            Member member = new Member();
            member.setId(memberview1.getTfidentitas().getText().trim());
            member.setNama(memberview1.getTfnama().getText().trim().toUpperCase());
            member.setNomorhandphone(memberview1.getTfhandphone().getText().trim().toUpperCase());
            member.setAlamat(memberview1.getTfalamat().getText().trim().toUpperCase());
            serviceMember.insertMember(member);
            JOptionPane.showMessageDialog(memberview1, "Data Member Berhasil Ditambahkan.", "Tambah Data Berhasil", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public void update() {
        if (memberview2.getTffidentitas().getText().isEmpty() || memberview2.getTffNama().getText().isEmpty() || memberview2.getTffHandpjone().getText().isEmpty()
                || memberview2.getTffAlamat().getText().isEmpty()) {
            JOptionPane.showMessageDialog(memberview2, "Form Harap Diisi lengkap & Tidak Kosong.", "Kesalahan", JOptionPane.WARNING_MESSAGE);
        } else {
            Member member = new Member();
            member.setId(memberview2.getTffidentitas().getText().trim().toUpperCase());
            member.setNama(memberview2.getTffNama().getText().trim().toUpperCase());
            member.setNomorhandphone(memberview2.getTffHandpjone().getText().trim().toUpperCase());
            member.setAlamat(memberview2.getTffAlamat().getText().trim().toUpperCase());
            serviceMember.updateMember(member);
            JOptionPane.showMessageDialog(memberview2, "Data Member Berhasil Disunting.", "Tambah Data Berhasil", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public void delete() {
        if (memberview2.getTffidentitas().getText().isEmpty()) {
            JOptionPane.showMessageDialog(memberview2, "Pastikan Field Identitas Tidak Kosong.", "Kesalahan", JOptionPane.WARNING_MESSAGE);
        } else {
            String id = memberview2.getTffidentitas().getText().trim();
            int hapus = JOptionPane.showConfirmDialog(memberview2, "Apakah anda yakin akan menghapus data " + id + " ?", "Konfirmasi Hapus", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
            if (hapus == JOptionPane.YES_OPTION) {
                    serviceMember.deleteMember(id);
                    JOptionPane.showMessageDialog(memberview2, "Data " + id + " berhasil dihapus.");
            }
        }
    }

    public void isiTable() {
        daftarMember = serviceMember.getAllMember();
        Object[][] data = new Object[daftarMember.size()][4];
        int baris = 0;
        for (Iterator<Member> it = daftarMember.iterator(); it.hasNext();) {
            Member member = it.next();
            data[baris][0] = member.getId();
            data[baris][1] = member.getNama();
            data[baris][2] = member.getNomorhandphone();
            data[baris][3] = member.getAlamat();
            baris++;
        }
        String header[] = {"NOMOR IDENTITAS", "NAMA", "NO. HANDPHONE", "ALAMAT"};
        DefaultTableModel model = new DefaultTableModel(data, header);
        memberview2.getTableMember().setModel(model);
    }
    
    public void Pencarian(String keyword){
        daftarMember = serviceMember.getSearchMember(keyword);
        Object[][] data = new Object[daftarMember.size()][4];
        int baris = 0;
        for (Iterator<Member> it = daftarMember.iterator(); it.hasNext();) {
            Member member = it.next();
            data[baris][0] = member.getId();
            data[baris][1] = member.getNama();
            data[baris][2] = member.getNomorhandphone();
            data[baris][3] = member.getAlamat();
            baris++;
        }
        String header[] = {"NOMOR IDENTITAS", "NAMA", "NO. HANDPHONE", "ALAMAT"};
        DefaultTableModel model = new DefaultTableModel(data, header);
        memberview2.getTableMember().setModel(model);
    }

    public void kosongkanFormLihat() {
        memberview2.getTffidentitas().setText("");
        memberview2.getTffNama().setText("");
        memberview2.getTffAlamat().setText("");
        memberview2.getTffHandpjone().setText("");
        memberview2.getTffPencarian().setText("");
    }

    public void kosongkanFormTambah() {
        memberview1.getTfidentitas().setText("");
        memberview1.getTfnama().setText("");
        memberview1.getTfalamat().setText("");
        memberview1.getTfhandphone().setText("");
    }

    public void isiForm(int row) {
        memberview2.getTffidentitas().setText(daftarMember.get(row).getId());
        memberview2.getTffNama().setText(daftarMember.get(row).getNama());
        memberview2.getTffAlamat().setText(daftarMember.get(row).getAlamat());
        memberview2.getTffHandpjone().setText(daftarMember.get(row).getNomorhandphone());
    }
}
