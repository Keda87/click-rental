package id.keda87.clickrental.controller;

import id.keda87.clickrental.dao.ConcreteStaffDao;
import id.keda87.clickrental.model.Staff;
import id.keda87.clickrental.view.sub.SubStaff;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class ControllerStaff {

    SubStaff panel;
    ConcreteStaffDao serviceStaff;
    List<Staff> daftarStaff;

    public ControllerStaff(SubStaff panel) {
        this.panel = panel;
        serviceStaff = new ConcreteStaffDao();
        try {
            daftarStaff = serviceStaff.getAllStaff();
        } catch (SQLException ex) {
            Logger.getLogger(ControllerStaff.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void insert() {
        if (panel.getTfID().getText().isEmpty() || panel.getTfNAMA().getText().isEmpty() || panel.getTfHP().getText().isEmpty() || panel.getTfGAJI().getText().isEmpty()
                || panel.getTaALAMAT().getText().isEmpty() || panel.getCbJABATAN().getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(panel, "Form Harap Diisi lengkap & Tidak Kosong.", "Kesalahan", JOptionPane.WARNING_MESSAGE);
        } else {
            try {
                Staff staff = new Staff();
                staff.setId(panel.getTfID().getText().toUpperCase());
                staff.setNama(panel.getTfNAMA().getText().toUpperCase());
                staff.setJabatan(panel.getCbJABATAN().getSelectedItem().toString().toUpperCase());
                staff.setAlamat(panel.getTaALAMAT().getText().toUpperCase());
                staff.setNomorHp(panel.getTfHP().getText().toUpperCase());
                staff.setGaji(Double.parseDouble(panel.getTfGAJI().getText()));
                serviceStaff.insertStaff(staff);
                JOptionPane.showMessageDialog(panel, "Data Staff Berhasil Ditambahkan.", "Tambah Data Berhasil", JOptionPane.INFORMATION_MESSAGE);
            } catch (SQLException ex) {
                Logger.getLogger(ControllerStaff.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(panel, "Terjadi Kesalahan MySQL / Kesalahan Format Pengisian Form.", "Kesalahan", JOptionPane.WARNING_MESSAGE);
            }
        }

    }

    public void update() {
        if (panel.getTfID().getText().isEmpty() || panel.getTfNAMA().getText().isEmpty() || panel.getTfHP().getText().isEmpty() || panel.getTfGAJI().getText().isEmpty()
                || panel.getTaALAMAT().getText().isEmpty() || panel.getCbJABATAN().getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(panel, "Form Harap Diisi lengkap & Tidak Kosong.", "Kesalahan", JOptionPane.WARNING_MESSAGE);
        } else {
            try {
                Staff staff = new Staff();
                staff.setId(panel.getTfID().getText().toUpperCase());
                staff.setNama(panel.getTfNAMA().getText().toUpperCase());
                staff.setJabatan(panel.getCbJABATAN().getSelectedItem().toString().toUpperCase());
                staff.setAlamat(panel.getTaALAMAT().getText().toUpperCase());
                staff.setNomorHp(panel.getTfHP().getText().toUpperCase());
                staff.setGaji(Double.parseDouble(panel.getTfGAJI().getText()));
                serviceStaff.updataStaff(staff);
            } catch (SQLException ex) {
                Logger.getLogger(ControllerStaff.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void delete() {
        try {
            String pk = panel.getTfID().getText().trim();
            int eksekusi = JOptionPane.showConfirmDialog(panel, "Apakah anda yakin akan menghapus data " + pk + " ?", "Konfirmasi Hapus", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
            if (eksekusi == JOptionPane.YES_OPTION) {
                serviceStaff.deleteStaff(pk);
                JOptionPane.showMessageDialog(panel, "Data " + pk + " berhasil dihapus.");
            }
        } catch (SQLException ex) {
            Logger.getLogger(ControllerStaff.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void kosongkanForm() {
        panel.getTfID().setText("");
        panel.getTfNAMA().setText("");
        panel.getTaALAMAT().setText("");
        panel.getCbJABATAN().setSelectedIndex(-1);
        panel.getTfHP().setText("");
        panel.getTfGAJI().setText("");
        panel.getTfPencarian().setText("");
    }

    public void isiForm(int row) {
        panel.getTfID().setText(daftarStaff.get(row).getId());
        panel.getTfNAMA().setText(daftarStaff.get(row).getNama());
        panel.getTaALAMAT().setText(daftarStaff.get(row).getAlamat());
        panel.getCbJABATAN().setSelectedItem(daftarStaff.get(row).getJabatan());
        panel.getTfHP().setText(daftarStaff.get(row).getNomorHp());
        panel.getTfGAJI().setText(daftarStaff.get(row).getGaji().toString());
    }

    public void isiTable() {
        try {
            daftarStaff = serviceStaff.getAllStaff();
            Object[][] data = new Object[daftarStaff.size()][6];
            int baris = 0;
            for (Staff staff : daftarStaff) {
                data[baris][0] = staff.getId();
                data[baris][1] = staff.getNama();
                data[baris][2] = staff.getJabatan();
                data[baris][3] = staff.getAlamat();
                data[baris][4] = staff.getNomorHp();
                data[baris][5] = formatUang(staff.getGaji());
                baris++;
            }
            String[] header = {"ID STAFF", "NAMA", "JABATAN", "ALAMAT", "NO. HANDPHONE", "GAJI"};
            DefaultTableModel model = new DefaultTableModel(data, header);
            panel.getTabel_staff().setModel(model);
        } catch (SQLException ex) {
            Logger.getLogger(ControllerStaff.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void pencarian(String cari) {
        try {
            daftarStaff = serviceStaff.getSearchStaff(cari);
            Object[][] data = new Object[daftarStaff.size()][6];
            int baris = 0;
            for (Staff staff : daftarStaff) {
                data[baris][0] = staff.getId();
                data[baris][1] = staff.getNama();
                data[baris][2] = staff.getJabatan();
                data[baris][3] = staff.getAlamat();
                data[baris][4] = staff.getNomorHp();
                data[baris][5] = formatUang(staff.getGaji());
                baris++;
            }
            String[] header = {"ID STAFF", "NAMA", "JABATAN", "ALAMAT", "NO. HANDPHONE", "GAJI"};
            DefaultTableModel modelcari = new DefaultTableModel(data, header);
            panel.getTabel_staff().setModel(modelcari);
        } catch (SQLException ex) {
            Logger.getLogger(ControllerStaff.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void kondisiSemula() {
        panel.getButtonINSERT().setEnabled(true);
        panel.getButtonDELETE().setEnabled(false);
        panel.getButtonRESET().setEnabled(true);
        panel.getButtonUPDATE().setEnabled(false);
        panel.getTfID().setEnabled(true);
    }

    public void kondisiSeleksi() {
        panel.getButtonINSERT().setEnabled(false);
        panel.getButtonDELETE().setEnabled(true);
        panel.getButtonRESET().setEnabled(true);
        panel.getButtonUPDATE().setEnabled(true);
        panel.getTfID().setEnabled(false);
    }

    public static String formatUang(Double n) {
        DecimalFormat df = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setCurrencySymbol("Rp. ");
        dfs.setMonetaryDecimalSeparator(',');
        dfs.setGroupingSeparator('.');
        df.setDecimalFormatSymbols(dfs);
        String convert = df.format(n);
        return convert;
    }
}
