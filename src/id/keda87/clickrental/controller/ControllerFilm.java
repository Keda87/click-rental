package id.keda87.clickrental.controller;

import id.keda87.clickrental.dao.ConcreteFilmDao;
import id.keda87.clickrental.model.Film;
import id.keda87.clickrental.view.sub.SubFilm;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class ControllerFilm {

    private SubFilm viewfilm;
    private ConcreteFilmDao serviceFilm;
    List<Film> daftarFilm;

    public ControllerFilm(SubFilm viewfilm) {
        this.viewfilm = viewfilm;
        serviceFilm = new ConcreteFilmDao();
        daftarFilm = serviceFilm.getAllFilm();
    }

    public void insert() {
        if (viewfilm.getTfNAMAFILM().getText().isEmpty() || viewfilm.getTfPRODUKSI().getText().isEmpty()
                || viewfilm.getTfTAHUNRILIS().getText().isEmpty() || viewfilm.getCbSTATUS().getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(viewfilm, "Form Harap Diisi lengkap & Tidak Kosong.", "Kesalahan", JOptionPane.WARNING_MESSAGE);
        } else {
            Film film = new Film();
            film.setNamaFilm(viewfilm.getTfNAMAFILM().getText().toUpperCase());
            film.setProduksi(viewfilm.getTfPRODUKSI().getText().toUpperCase());
            film.setTahunTerbit(viewfilm.getTfTAHUNRILIS().getText().toUpperCase());
            film.setStatusKembali(viewfilm.getCbSTATUS().getSelectedItem().toString());
            try {
                serviceFilm.insertFilm(film);
                JOptionPane.showMessageDialog(viewfilm, "Data Film Berhasil Ditambahkan.", "Tambah Data Berhasil", JOptionPane.INFORMATION_MESSAGE);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(viewfilm, "Data Gagal Ditambahkan, Terjadi Kesalahan Pada MySQL.", "Kesalahan", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    public void update() {
        if (viewfilm.getTfIDFILM().getText().isEmpty() || viewfilm.getTfNAMAFILM().getText().isEmpty() || viewfilm.getTfPRODUKSI().getText().isEmpty()
                || viewfilm.getTfTAHUNRILIS().getText().isEmpty() || viewfilm.getCbSTATUS().getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(viewfilm, "Form Harap Diisi lengkap & Tidak Kosong.", "Kesalahan", JOptionPane.WARNING_MESSAGE);
        } else {
            Film film = new Film();
            film.setId(Integer.parseInt(viewfilm.getTfIDFILM().getText().toUpperCase().trim()));
            film.setNamaFilm(viewfilm.getTfNAMAFILM().getText().toUpperCase().trim());
            film.setProduksi(viewfilm.getTfPRODUKSI().getText().toUpperCase().trim());
            film.setTahunTerbit(viewfilm.getTfTAHUNRILIS().getText().toUpperCase().trim());
            film.setStatusKembali(viewfilm.getCbSTATUS().getSelectedItem().toString());
            try {
                serviceFilm.updateFilm(film);
                JOptionPane.showMessageDialog(viewfilm, "Data Film Berhasil Disunting.", "Tambah Data Berhasil", JOptionPane.INFORMATION_MESSAGE);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(viewfilm, "Data Gagal Disunting, Terjadi Kesalahan Pada MySQL.", "Kesalahan", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    public void delete() {
        if (viewfilm.getTfIDFILM().getText().isEmpty()) {
            JOptionPane.showMessageDialog(viewfilm, "Pastikan Field Identitas Film Tidak Kosong.", "Kesalahan", JOptionPane.WARNING_MESSAGE);
        } else {
            int id = Integer.parseInt(viewfilm.getTfIDFILM().getText().trim());
            int hapus = JOptionPane.showConfirmDialog(viewfilm, "Apakah anda yakin akan menghapus data " + id + " ?", "Konfirmasi Hapus", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
            if (hapus == JOptionPane.YES_OPTION) {
                serviceFilm.deleteFilm(id);
                JOptionPane.showMessageDialog(viewfilm, "Data " + id + " berhasil dihapus.");
            }
        }
    }

    public void kosongkanForm() {
        viewfilm.getTfIDFILM().setText("");
        viewfilm.getTfNAMAFILM().setText("");
        viewfilm.getTfPRODUKSI().setText("");
        viewfilm.getTfTAHUNRILIS().setText("");
        viewfilm.getCbSTATUS().setSelectedIndex(0);
        viewfilm.getTfPENCARIAN().setText("");
    }

    public void isiForm(int row) {
        viewfilm.getTfIDFILM().setText(String.valueOf(daftarFilm.get(row).getId()));
        viewfilm.getTfNAMAFILM().setText(daftarFilm.get(row).getNamaFilm());
        viewfilm.getTfPRODUKSI().setText(daftarFilm.get(row).getProduksi());
        viewfilm.getTfTAHUNRILIS().setText(daftarFilm.get(row).getTahunTerbit());
        viewfilm.getCbSTATUS().setSelectedItem(daftarFilm.get(row).getStatusKembali());
    }

    public void isiTable() {
        daftarFilm = serviceFilm.getAllFilm();
        Object[][] data = new Object[daftarFilm.size()][5];
        int baris = 0;
        for (Film film : daftarFilm) {
            data[baris][0] = film.getId();
            data[baris][1] = film.getNamaFilm();
            data[baris][2] = film.getTahunTerbit();
            data[baris][3] = film.getProduksi();
            data[baris][4] = film.getStatusKembali();
            baris++;
        }
        String[] header = {"ID FILM", "NAMA FILM", "TAHUN TERBIT", "PRODUKSI", "STATUS"};
        DefaultTableModel model = new DefaultTableModel(data, header);
        viewfilm.getTabelFILM().setModel(model);
    }

    public void Pencarian(String keyword) {
        daftarFilm = serviceFilm.getSearchFilm(keyword);
        Object[][] data = new Object[daftarFilm.size()][5];
        int baris = 0;
        for (Film film : daftarFilm) {
            data[baris][0] = film.getId();
            data[baris][1] = film.getNamaFilm();
            data[baris][2] = film.getTahunTerbit();
            data[baris][3] = film.getProduksi();
            data[baris][4] = film.getStatusKembali();
            baris++;
        }
        String[] header = {"ID FILM", "NAMA FILM", "TAHUN TERBIT", "PRODUKSI", "STATUS"};
        DefaultTableModel model = new DefaultTableModel(data, header);
        viewfilm.getTabelFILM().setModel(model);
    }

    public void kondisiSemula() {
        viewfilm.getButtonSUNTING().setEnabled(false);
        viewfilm.getButtonHAPUS().setEnabled(false);
        viewfilm.getButtonSEGARKAN().setEnabled(true);
        viewfilm.getButtonTAMBAH().setEnabled(true);
    }

    public void kondisiSeleksi() {
        viewfilm.getButtonSUNTING().setEnabled(true);
        viewfilm.getButtonHAPUS().setEnabled(true);
        viewfilm.getButtonSEGARKAN().setEnabled(true);
        viewfilm.getButtonTAMBAH().setEnabled(false);
    }
}
