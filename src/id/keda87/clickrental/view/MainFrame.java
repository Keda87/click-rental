/*
 * DILARANG MENGHAPUS ATAU MENGEDIT COPYRIGHT INI.
 * 
 * Copyright 2013 ADIYAT MUBARAK. 
 * All rights reserved.
 * 
 * Semua isi dalam file ini adalah hak milik dari ADIYAT MUBARAK
 * Anda tak diperkenankan untuk menggunakan file atau mengubah file
 * ini kecuali anda tidak menghapus atau merubah lisence ini.
 * 
 * File ini dibuat menggunakan :
 * IDE        : NetBeans
 * NoteBook   : Toshiba Satellite L745
 * OS         : Windows 7
 * Java       : Java 1.7
 */
package id.keda87.clickrental.view;

import de.javasoft.plaf.synthetica.SyntheticaAluOxideLookAndFeel;
import id.keda87.clickrental.view.sub.SubChart;
import id.keda87.clickrental.view.sub.SubFilm;
import id.keda87.clickrental.view.sub.SubLihatMember;
import id.keda87.clickrental.view.sub.SubPeminjaman;
import id.keda87.clickrental.view.sub.SubPengembalian;
import id.keda87.clickrental.view.sub.SubSettingHarga;
import id.keda87.clickrental.view.sub.SubStaff;
import id.keda87.clickrental.view.sub.SubTambahMember;
import java.awt.CardLayout;
import java.text.ParseException;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.UIManager;

public class MainFrame extends javax.swing.JFrame {

    private JPanel kosong = new JPanel();
    private SubStaff sStaff = new SubStaff();
    private SubPeminjaman sPinjam = new SubPeminjaman();
    private SubPengembalian sKembali = new SubPengembalian();
    private SubSettingHarga sSetting = new SubSettingHarga();
    private SubFilm sFilm = new SubFilm();
    private SubTambahMember sTMember = new SubTambahMember();
    private SubLihatMember sLMember = new SubLihatMember();
    private SubChart sChart = new SubChart();

    public MainFrame() {
        super("Click Rental v.0.1");
        initComponents();
        setLocationRelativeTo(null);

        display_panel.add(kosong, "panelkosong");
        display_panel.add(sStaff, "panelstaff");
        display_panel.add(sTMember, "panelmember1");
        display_panel.add(sLMember, "panelmember2");
        display_panel.add(sFilm, "panelfilm");
        display_panel.add(sPinjam, "panelpeminjaman");
        display_panel.add(sSetting, "panelnominal");
        display_panel.add(sKembali, "panelpengembalian");
        display_panel.add(sChart, "panelgrafik");
        setIconImage(new ImageIcon("src\\id\\keda87\\clickrental\\icon\\bill-48.png").getImage());
        setGlassPane(glass);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        display_panel = new javax.swing.JPanel();
        glass = new id.keda87.clickrental.view.sub.GlassPaneTransition();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        StaffAdm = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        MenuPeminjaman = new javax.swing.JMenuItem();
        MenuPengembalian = new javax.swing.JMenuItem();
        MenuGrafik = new javax.swing.JMenuItem();
        MenuPengaturan = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenu5 = new javax.swing.JMenu();
        MenuTambahMember = new javax.swing.JMenuItem();
        MenuLihatMember = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        MenuDataFilm = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        display_panel.setLayout(new java.awt.CardLayout());
        display_panel.add(glass, "card2");

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/id/keda87/clickrental/icon/staff.png"))); // NOI18N
        jMenu1.setText("Staff");
        jMenu1.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N

        StaffAdm.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        StaffAdm.setIcon(new javax.swing.ImageIcon(getClass().getResource("/id/keda87/clickrental/icon/karyawan.png"))); // NOI18N
        StaffAdm.setText("Administrasi Staff");
        StaffAdm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                StaffAdmActionPerformed(evt);
            }
        });
        jMenu1.add(StaffAdm);

        jMenuBar1.add(jMenu1);

        jMenu2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/id/keda87/clickrental/icon/transaction.png"))); // NOI18N
        jMenu2.setText("Transaksi");
        jMenu2.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N

        MenuPeminjaman.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        MenuPeminjaman.setIcon(new javax.swing.ImageIcon(getClass().getResource("/id/keda87/clickrental/icon/peminjaman.png"))); // NOI18N
        MenuPeminjaman.setText("Peminjaman");
        MenuPeminjaman.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuPeminjamanActionPerformed(evt);
            }
        });
        jMenu2.add(MenuPeminjaman);

        MenuPengembalian.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        MenuPengembalian.setIcon(new javax.swing.ImageIcon(getClass().getResource("/id/keda87/clickrental/icon/pengembalian.png"))); // NOI18N
        MenuPengembalian.setText("Pengembalian");
        MenuPengembalian.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuPengembalianActionPerformed(evt);
            }
        });
        jMenu2.add(MenuPengembalian);

        MenuGrafik.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        MenuGrafik.setIcon(new javax.swing.ImageIcon(getClass().getResource("/id/keda87/clickrental/icon/Line-chart-icon.png"))); // NOI18N
        MenuGrafik.setText("Grafik Penyewaan");
        MenuGrafik.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuGrafikActionPerformed(evt);
            }
        });
        jMenu2.add(MenuGrafik);

        MenuPengaturan.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        MenuPengaturan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/id/keda87/clickrental/icon/Settings-icon.png"))); // NOI18N
        MenuPengaturan.setText("Pengaturan");
        MenuPengaturan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuPengaturanActionPerformed(evt);
            }
        });
        jMenu2.add(MenuPengaturan);

        jMenuBar1.add(jMenu2);

        jMenu3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/id/keda87/clickrental/icon/admin member.png"))); // NOI18N
        jMenu3.setText("Member");

        jMenu5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/id/keda87/clickrental/icon/people (1).png"))); // NOI18N
        jMenu5.setText("Administrasi Member");

        MenuTambahMember.setIcon(new javax.swing.ImageIcon(getClass().getResource("/id/keda87/clickrental/icon/tambah member.png"))); // NOI18N
        MenuTambahMember.setText("Tambah Member Baru");
        MenuTambahMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuTambahMemberActionPerformed(evt);
            }
        });
        jMenu5.add(MenuTambahMember);

        MenuLihatMember.setIcon(new javax.swing.ImageIcon(getClass().getResource("/id/keda87/clickrental/icon/lihat member.png"))); // NOI18N
        MenuLihatMember.setText("Lihat Data Member");
        MenuLihatMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuLihatMemberActionPerformed(evt);
            }
        });
        jMenu5.add(MenuLihatMember);

        jMenu3.add(jMenu5);

        jMenuBar1.add(jMenu3);

        jMenu4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/id/keda87/clickrental/icon/film.png"))); // NOI18N
        jMenu4.setText("Film");

        MenuDataFilm.setIcon(new javax.swing.ImageIcon(getClass().getResource("/id/keda87/clickrental/icon/data film.png"))); // NOI18N
        MenuDataFilm.setText("Data Film");
        MenuDataFilm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuDataFilmActionPerformed(evt);
            }
        });
        jMenu4.add(MenuDataFilm);

        jMenuBar1.add(jMenu4);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(display_panel, javax.swing.GroupLayout.DEFAULT_SIZE, 1013, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(display_panel, javax.swing.GroupLayout.DEFAULT_SIZE, 481, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void StaffAdmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_StaffAdmActionPerformed
        if (glass.isProgress() == false) {
            glass.setAnimatedStep(5);
            glass.startTransition(display_panel);
            CardLayout cl = (CardLayout) display_panel.getLayout();
            cl.show(display_panel, "panelstaff");
        }
    }//GEN-LAST:event_StaffAdmActionPerformed

    private void MenuTambahMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuTambahMemberActionPerformed
        if (glass.isProgress() == false) {
            glass.setAnimatedStep(5);
            glass.startTransition(display_panel);
            CardLayout cl = (CardLayout) display_panel.getLayout();
            cl.show(display_panel, "panelmember1");
        }
    }//GEN-LAST:event_MenuTambahMemberActionPerformed

    private void MenuLihatMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuLihatMemberActionPerformed
        if (glass.isProgress() == false) {
            glass.setAnimatedStep(5);
            glass.startTransition(display_panel);
            CardLayout cl = (CardLayout) display_panel.getLayout();
            cl.show(display_panel, "panelmember2");
        }
    }//GEN-LAST:event_MenuLihatMemberActionPerformed

    private void MenuDataFilmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuDataFilmActionPerformed
        if (glass.isProgress() == false) {
            glass.setAnimatedStep(5);
            glass.startTransition(display_panel);
            CardLayout cl = (CardLayout) display_panel.getLayout();
            cl.show(display_panel, "panelfilm");
        }
    }//GEN-LAST:event_MenuDataFilmActionPerformed

    private void MenuPeminjamanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuPeminjamanActionPerformed
        if (glass.isProgress() == false) {
            glass.setAnimatedStep(5);
            glass.startTransition(display_panel);
            CardLayout cl = (CardLayout) display_panel.getLayout();
            cl.show(display_panel, "panelpeminjaman");
        }
    }//GEN-LAST:event_MenuPeminjamanActionPerformed

    private void MenuPengaturanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuPengaturanActionPerformed
        if (glass.isProgress() == false) {
            glass.setAnimatedStep(5);
            glass.startTransition(display_panel);
            CardLayout cl = (CardLayout) display_panel.getLayout();
            cl.show(display_panel, "panelnominal");
        }
    }//GEN-LAST:event_MenuPengaturanActionPerformed

    private void MenuPengembalianActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuPengembalianActionPerformed
        if (glass.isProgress() == false) {
            glass.setAnimatedStep(5);
            glass.startTransition(display_panel);
            CardLayout cl = (CardLayout) display_panel.getLayout();
            cl.show(display_panel, "panelpengembalian");
        }
    }//GEN-LAST:event_MenuPengembalianActionPerformed

    private void MenuGrafikActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuGrafikActionPerformed
        if (glass.isProgress() == false) {
            glass.setAnimatedStep(5);
            glass.startTransition(display_panel);
            CardLayout cl = (CardLayout) display_panel.getLayout();
            cl.show(display_panel, "panelgrafik");
        }
    }//GEN-LAST:event_MenuGrafikActionPerformed

    public static void main(String args[]) {
        try {
            UIManager.setLookAndFeel(new SyntheticaAluOxideLookAndFeel());
        } catch (javax.swing.UnsupportedLookAndFeelException | ParseException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem MenuDataFilm;
    private javax.swing.JMenuItem MenuGrafik;
    private javax.swing.JMenuItem MenuLihatMember;
    private javax.swing.JMenuItem MenuPeminjaman;
    private javax.swing.JMenuItem MenuPengaturan;
    private javax.swing.JMenuItem MenuPengembalian;
    private javax.swing.JMenuItem MenuTambahMember;
    private javax.swing.JMenuItem StaffAdm;
    private javax.swing.JPanel display_panel;
    private id.keda87.clickrental.view.sub.GlassPaneTransition glass;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenuBar jMenuBar1;
    // End of variables declaration//GEN-END:variables
}
