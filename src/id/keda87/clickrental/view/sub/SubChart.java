package id.keda87.clickrental.view.sub;

import id.keda87.clickrental.dao.ConcreteTransaksiDao;
import id.keda87.clickrental.model.Transaksi;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

public class SubChart extends JPanel {

    List<Transaksi> trn = new ArrayList<>();
    ConcreteTransaksiDao serviceTransaksiDao;

    public SubChart() {
        serviceTransaksiDao = new ConcreteTransaksiDao();
        trn = serviceTransaksiDao.tampilkanGrafik();
        DefaultCategoryDataset dcd = new DefaultCategoryDataset();
        for (Transaksi transaksi : trn) {
            dcd.setValue(transaksi.getTotalTransaksi(), transaksi.getTanggalPinjam(), "");
        }
        JFreeChart chart = ChartFactory.createBarChart3D("Grafik Persewaan DVD/Bulan", "Keterangan Bulan", "Jumlah", dcd, PlotOrientation.VERTICAL, true, true, true);
        ChartPanel cp = new ChartPanel(chart);
        add(cp);
    }
}
