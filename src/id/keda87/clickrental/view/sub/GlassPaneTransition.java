package id.keda87.clickrental.view.sub;

/**
 * GlassPaneTransition.java 1.0 22 Des 07 at 21:34:49
 *
 * Copyright 2007 Echo, All rights reserved. http://eecchhoo.wordpress.com/
 * echo.khannedy@gmail.com
 */
import java.awt.AWTException;
import java.awt.AlphaComposite;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

/**
 * {@link GlassPaneTransition} merupakan class turunan dari {@link JComponent}
 * yang berguna sebagai glasspane yang dapat menampilkan animasi transisi
 *
 * @author echo
 * @version 1.0.0
 * @since 22 Desember 2007
 * @see <a href="http://eecchhoo.wordpress.com/">Echo Website</a>
 */
public class GlassPaneTransition extends JComponent {

    /**
     * Serial
     *
     * @see Serializable
     */
    private static final long serialVersionUID = 1560086616120588875L;
    /**
     * int alpha
     */
    private int alpha;
    /**
     * {@link Component} yang menjadi dasar transisi
     */
    private Component comp;
    /**
     * Gambar yang mendaji transisi
     */
    private BufferedImage image;
    /**
     * Variable yang menyatakan apakah component sedang menjalankan transisi
     * atau tidak
     */
    private boolean inProgress;
    /**
     * Robot yang mencapture gambar
     */
    private Robot rbt;
    /**
     * Step transisi
     */
    private int step = 5;
    /**
     * Pengatur waktu transisi;
     */
    private Timer timer;

    /**
     * Membuat {@link GlassPaneTransition} baru
     */
    public GlassPaneTransition() {
        super();
        try {
            rbt = new Robot();
            timer = new Timer(10, new ActionListener() {
                @SuppressWarnings(value = "synthetic-access")
                @Override
                public void actionPerformed(
                        @SuppressWarnings(value = "unused")
                        final ActionEvent e) {
                    alpha -= step;
                    repaint();
                    if (alpha <= 0) {
                        timer.stop();
                        inProgress = false;
                        setVisible(false);
                        image = null;
                    }
                }
            });
        } catch (final AWTException ex) {
            Logger.getLogger(GlassPaneTransition.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metode ini mengembalikan nilai boolean trus jika component sedang
     * menjalankan transisi, dan false jika tidak sedang menjalankan transisi
     *
     * @return boolean apakah component sedang manjalankan transisi
     */
    public boolean isProgress() {
        return inProgress;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
     */
    @Override
    protected void paintComponent(final Graphics g) {
        super.paintComponent(g);
        if (image != null) {
            final Graphics2D g2 = (Graphics2D) g.create();
            final Point p = SwingUtilities.convertPoint(comp, 0, 0, this);
            g2.setComposite(AlphaComposite.SrcOver.derive(alpha / 100f));
            g2.drawImage(image, (int) p.getX(), (int) p.getY(), null);
        }
    }

    /**
     * @param i
     */
    public void setAnimatedStep(final int i) {
        step = i;
    }

    /**
     * Memulai transisi diatas component
     *
     * @param comp Component yang akan dijadikan latar transisi
     */
    public void startTransition(final Component comp) {
        this.comp = comp;
        alpha = 100;
        inProgress = true;
        image = rbt.createScreenCapture(new Rectangle(this.comp.getLocationOnScreen(), this.comp.getSize()));
        timer.start();
        setVisible(true);
    }
}
