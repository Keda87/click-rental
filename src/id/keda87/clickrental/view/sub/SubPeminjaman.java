/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package id.keda87.clickrental.view.sub;

import com.toedter.calendar.JDateChooser;
import id.keda87.clickrental.controller.ControllerTransaksi;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author Keda87
 */
public class SubPeminjaman extends javax.swing.JPanel {

    private ControllerTransaksi ctr;
    private DefaultComboBoxModel modelComboID;
    private DefaultComboBoxModel modelComboFilm;

    public SubPeminjaman() {
        initComponents();
        ctr = new ControllerTransaksi(this);
        modelComboID = new DefaultComboBoxModel();
        modelComboFilm = new DefaultComboBoxModel();
        tfidmember.setModel(modelComboID);
        tfnamafilm.setModel(modelComboFilm);
        ctr.isiComboNamaFilm();
        ctr.isiComboIDMember();
    }

    public ControllerTransaksi getCtr() {
        return ctr;
    }

    public DefaultComboBoxModel getModelComboID() {
        return modelComboID;
    }

    public DefaultComboBoxModel getModelComboFilm() {
        return modelComboFilm;
    }

    public JCheckBox getTfCekMember() {
        return tfCekMember;
    }

    public JTextArea getTfalamat() {
        return tfalamat;
    }

    public JTextField getTfidfilm() {
        return tfidfilm;
    }

    public JComboBox getTfidmember() {
        return tfidmember;
    }

    public JTextField getTfidtransaksi() {
        return tfidtransaksi;
    }

    public JTextField getTfkembalian() {
        return tfkembalian;
    }

    public JComboBox getTfnamafilm() {
        return tfnamafilm;
    }

    public JTextField getTfnamapenyewa() {
        return tfnamapenyewa;
    }

    public JTextField getTfnohandphone() {
        return tfnohandphone;
    }

    public JTextField getTfpembayaran() {
        return tfpembayaran;
    }

    public JDateChooser getTftanggalkembali() {
        return tftanggalkembali;
    }

    public JDateChooser getTftanggalpinjam() {
        return tftanggalpinjam;
    }

    public JTextField getTftotaltransaksi() {
        return tftotaltransaksi;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        tfidtransaksi = new javax.swing.JTextField();
        tfCekMember = new javax.swing.JCheckBox();
        tfidmember = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        tfnamapenyewa = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        tfnohandphone = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tfalamat = new javax.swing.JTextArea();
        jLabel8 = new javax.swing.JLabel();
        tfnamafilm = new javax.swing.JComboBox();
        tfidfilm = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        tftanggalpinjam = new com.toedter.calendar.JDateChooser();
        jLabel11 = new javax.swing.JLabel();
        tftanggalkembali = new com.toedter.calendar.JDateChooser();
        buttonProses = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        tftotaltransaksi = new javax.swing.JTextField();
        tfpembayaran = new javax.swing.JTextField();
        tfkembalian = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        buttonSIMPANTRANSAKSI = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        jLabel1.setText("Menu Transaksi Peminjaman");

        jLabel2.setText("ID Transaksi");

        jLabel3.setText("ID Member");

        tfidtransaksi.setEnabled(false);

        tfCekMember.setText("Member");
        tfCekMember.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                tfCekMemberStateChanged(evt);
            }
        });

        tfidmember.setEnabled(false);
        tfidmember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfidmemberActionPerformed(evt);
            }
        });

        jLabel4.setText("*ID OTOMATIS DIBUAT");

        jLabel5.setText("Nama");

        jLabel6.setText("No. Handphone");

        jLabel7.setText("Alamat");

        tfalamat.setColumns(20);
        tfalamat.setLineWrap(true);
        tfalamat.setRows(5);
        jScrollPane1.setViewportView(tfalamat);

        jLabel8.setText("Nama Film");

        tfnamafilm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfnamafilmActionPerformed(evt);
            }
        });

        tfidfilm.setEnabled(false);

        jLabel9.setText("ID Film");

        jLabel10.setText("Tanggal Pinjam");

        jLabel11.setText("Tanggal Kembali");

        buttonProses.setText("Proses");
        buttonProses.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonProsesActionPerformed(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        jLabel12.setText("Detail Transaksi");

        tftotaltransaksi.setEnabled(false);

        tfkembalian.setEnabled(false);

        jLabel13.setText("Total Transaksi");

        jLabel14.setText("Pembayaran");

        jLabel15.setText("Kembalian");

        buttonSIMPANTRANSAKSI.setText("Simpan Transaksi");
        buttonSIMPANTRANSAKSI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSIMPANTRANSAKSIActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(tfidfilm)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(tfCekMember)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tfidmember, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(tfidtransaksi, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel4))
                            .addComponent(tfnamapenyewa)
                            .addComponent(tfnohandphone)
                            .addComponent(jScrollPane1)
                            .addComponent(tfnamafilm, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel13, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel15, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(tftotaltransaksi)
                                    .addComponent(tfpembayaran)
                                    .addComponent(tfkembalian, javax.swing.GroupLayout.DEFAULT_SIZE, 334, Short.MAX_VALUE)))
                            .addComponent(buttonSIMPANTRANSAKSI, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel10)
                                    .addComponent(jLabel11))
                                .addGap(16, 16, 16)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(tftanggalpinjam, javax.swing.GroupLayout.DEFAULT_SIZE, 165, Short.MAX_VALUE)
                                    .addComponent(tftanggalkembali, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(buttonProses, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(tfidtransaksi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(tfCekMember)
                    .addComponent(tfidmember, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(tfnamapenyewa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(tfnohandphone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(5, 5, 5)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(tfnamafilm, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tfidfilm, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel10)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel11)
                                .addGap(1, 1, 1))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(tftanggalpinjam, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(9, 9, 9)
                                .addComponent(tftanggalkembali, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(buttonProses, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tftotaltransaksi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel13))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tfpembayaran, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel14))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tfkembalian, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel15))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonSIMPANTRANSAKSI)))
                .addContainerGap(53, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void tfCekMemberStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_tfCekMemberStateChanged
        if (tfCekMember.isSelected()) {
            tfidmember.setEnabled(true);
        } else {
            tfidmember.setEnabled(false);
            tfidmember.setSelectedIndex(0);
            tfnamapenyewa.setText("");
            tfnohandphone.setText("");
            tfalamat.setText("");
        }
    }//GEN-LAST:event_tfCekMemberStateChanged

    private void tfidmemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfidmemberActionPerformed
        ctr.isiFormMember();
    }//GEN-LAST:event_tfidmemberActionPerformed

    private void tfnamafilmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfnamafilmActionPerformed
        ctr.isiIDFilm();
    }//GEN-LAST:event_tfnamafilmActionPerformed

    private void buttonProsesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonProsesActionPerformed
        ctr.isiSewa();
    }//GEN-LAST:event_buttonProsesActionPerformed

    private void buttonSIMPANTRANSAKSIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSIMPANTRANSAKSIActionPerformed
        ctr.hitungPembayaran();
        ctr.insert();
        ctr.kosongkanForm();
    }//GEN-LAST:event_buttonSIMPANTRANSAKSIActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonProses;
    private javax.swing.JButton buttonSIMPANTRANSAKSI;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JCheckBox tfCekMember;
    private javax.swing.JTextArea tfalamat;
    private javax.swing.JTextField tfidfilm;
    private javax.swing.JComboBox tfidmember;
    private javax.swing.JTextField tfidtransaksi;
    private javax.swing.JTextField tfkembalian;
    private javax.swing.JComboBox tfnamafilm;
    private javax.swing.JTextField tfnamapenyewa;
    private javax.swing.JTextField tfnohandphone;
    private javax.swing.JTextField tfpembayaran;
    private com.toedter.calendar.JDateChooser tftanggalkembali;
    private com.toedter.calendar.JDateChooser tftanggalpinjam;
    private javax.swing.JTextField tftotaltransaksi;
    // End of variables declaration//GEN-END:variables
}
