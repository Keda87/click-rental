package id.keda87.clickrental.view.sub;

import id.keda87.clickrental.controller.ControllerMember;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class SubTambahMember extends javax.swing.JPanel {

    private ControllerMember cmm;
    
    public SubTambahMember() {
        initComponents();
        cmm = new ControllerMember(this);
    }

    public JTextArea getTfalamat() {
        return tfalamat;
    }

    public JTextField getTfhandphone() {
        return tfhandphone;
    }

    public JTextField getTfidentitas() {
        return tfidentitas;
    }

    public JTextField getTfnama() {
        return tfnama;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        tfidentitas = new javax.swing.JTextField();
        tfnama = new javax.swing.JTextField();
        tfhandphone = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tfalamat = new javax.swing.JTextArea();
        buttonINSERT = new javax.swing.JButton();
        buttonREFRESH = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        jLabel1.setText("Menu Tambah Member");

        jLabel2.setText("Nomor Identitas");

        jLabel3.setText("Nama");

        jLabel4.setText("Nomor Handphone");

        jLabel5.setText("Alamat");

        tfalamat.setColumns(20);
        tfalamat.setLineWrap(true);
        tfalamat.setRows(5);
        jScrollPane1.setViewportView(tfalamat);

        buttonINSERT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/id/keda87/clickrental/icon/add_user-26.png"))); // NOI18N
        buttonINSERT.setText("Tambah");
        buttonINSERT.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonINSERT.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        buttonINSERT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonINSERTActionPerformed(evt);
            }
        });

        buttonREFRESH.setIcon(new javax.swing.ImageIcon(getClass().getResource("/id/keda87/clickrental/icon/change_user-26.png"))); // NOI18N
        buttonREFRESH.setText("Segarkan");
        buttonREFRESH.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonREFRESH.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        buttonREFRESH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonREFRESHActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(buttonREFRESH)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonINSERT))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(jLabel2))
                        .addGap(6, 6, 6)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tfidentitas)
                            .addComponent(tfhandphone)
                            .addComponent(tfnama)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 285, Short.MAX_VALUE))))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {buttonINSERT, buttonREFRESH});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(tfidentitas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(tfnama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(tfhandphone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buttonINSERT)
                    .addComponent(buttonREFRESH))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {buttonINSERT, buttonREFRESH});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void buttonINSERTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonINSERTActionPerformed
        cmm.insert();
        cmm.kosongkanFormTambah();
    }//GEN-LAST:event_buttonINSERTActionPerformed

    private void buttonREFRESHActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonREFRESHActionPerformed
        cmm.kosongkanFormTambah();
    }//GEN-LAST:event_buttonREFRESHActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonINSERT;
    private javax.swing.JButton buttonREFRESH;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea tfalamat;
    private javax.swing.JTextField tfhandphone;
    private javax.swing.JTextField tfidentitas;
    private javax.swing.JTextField tfnama;
    // End of variables declaration//GEN-END:variables
}
