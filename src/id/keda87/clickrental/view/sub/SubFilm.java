package id.keda87.clickrental.view.sub;

import id.keda87.clickrental.controller.ControllerFilm;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class SubFilm extends javax.swing.JPanel {

    private ControllerFilm cfl;

    public SubFilm() {
        initComponents();
        cfl = new ControllerFilm(this);
        cfl.isiTable();
        cfl.kondisiSemula();
        tabelFILM.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                int row = tabelFILM.getSelectedRow();
                if (row != -1) {
                    cfl.isiForm(row);
                    cfl.kondisiSeleksi();
                }
            }
        });
    }

    public JButton getButtonHAPUS() {
        return buttonHAPUS;
    }

    public JButton getButtonSEGARKAN() {
        return buttonSEGARKAN;
    }

    public JButton getButtonSUNTING() {
        return buttonSUNTING;
    }

    public JButton getButtonTAMBAH() {
        return buttonTAMBAH;
    }

    public JComboBox getCbSTATUS() {
        return cbSTATUS;
    }

    public JTable getTabelFILM() {
        return tabelFILM;
    }

    public JTextField getTfIDFILM() {
        return tfIDFILM;
    }

    public JTextField getTfNAMAFILM() {
        return tfNAMAFILM;
    }

    public JTextField getTfPENCARIAN() {
        return tfPENCARIAN;
    }

    public JTextField getTfPRODUKSI() {
        return tfPRODUKSI;
    }

    public JTextField getTfTAHUNRILIS() {
        return tfTAHUNRILIS;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        tfPENCARIAN = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelFILM = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        tfTAHUNRILIS = new javax.swing.JTextField();
        tfPRODUKSI = new javax.swing.JTextField();
        tfNAMAFILM = new javax.swing.JTextField();
        cbSTATUS = new javax.swing.JComboBox();
        tfIDFILM = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        buttonTAMBAH = new javax.swing.JButton();
        buttonSEGARKAN = new javax.swing.JButton();
        buttonSUNTING = new javax.swing.JButton();
        buttonHAPUS = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        jLabel1.setText("Menu Data Film");

        jLabel2.setText("Pencarian");

        tfPENCARIAN.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tfPENCARIANKeyTyped(evt);
            }
        });

        tabelFILM.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tabelFILM);

        jLabel3.setText("ID Film");

        jLabel4.setText("Nama Film");

        jLabel5.setText("Tahun Rilis");

        jLabel6.setText("Produksi");

        jLabel7.setText("Status");

        cbSTATUS.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "  ", "Tersedia", "Tidak Tersedia" }));

        tfIDFILM.setEnabled(false);

        jLabel8.setText("*ID OTOMATIS DIBUAT");

        buttonTAMBAH.setIcon(new javax.swing.ImageIcon(getClass().getResource("/id/keda87/clickrental/icon/plus-32.png"))); // NOI18N
        buttonTAMBAH.setText("Tambah");
        buttonTAMBAH.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonTAMBAH.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        buttonTAMBAH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonTAMBAHActionPerformed(evt);
            }
        });

        buttonSEGARKAN.setIcon(new javax.swing.ImageIcon(getClass().getResource("/id/keda87/clickrental/icon/recycle_sign-32.png"))); // NOI18N
        buttonSEGARKAN.setText("Segarkan");
        buttonSEGARKAN.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonSEGARKAN.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        buttonSEGARKAN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSEGARKANActionPerformed(evt);
            }
        });

        buttonSUNTING.setIcon(new javax.swing.ImageIcon(getClass().getResource("/id/keda87/clickrental/icon/time-32.png"))); // NOI18N
        buttonSUNTING.setText("Sunting");
        buttonSUNTING.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonSUNTING.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        buttonSUNTING.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSUNTINGActionPerformed(evt);
            }
        });

        buttonHAPUS.setIcon(new javax.swing.ImageIcon(getClass().getResource("/id/keda87/clickrental/icon/minus-32.png"))); // NOI18N
        buttonHAPUS.setText("Hapus");
        buttonHAPUS.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonHAPUS.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        buttonHAPUS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonHAPUSActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfPENCARIAN))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 570, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel3))
                                .addGap(24, 24, 24)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(tfPRODUKSI)
                                    .addComponent(tfTAHUNRILIS)
                                    .addComponent(tfNAMAFILM)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(tfIDFILM, javax.swing.GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel8))
                                    .addComponent(cbSTATUS, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(buttonSUNTING))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(buttonSEGARKAN)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(buttonTAMBAH)
                            .addComponent(buttonHAPUS, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {buttonHAPUS, buttonSEGARKAN, buttonSUNTING, buttonTAMBAH});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(tfPENCARIAN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel3)
                                    .addComponent(tfIDFILM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel8))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel4)
                                    .addComponent(tfNAMAFILM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel5)
                                    .addComponent(tfTAHUNRILIS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel6)
                                    .addComponent(tfPRODUKSI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(6, 6, 6)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel7)
                                    .addComponent(cbSTATUS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(buttonTAMBAH)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(buttonHAPUS, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(buttonSUNTING, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(buttonSEGARKAN, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {buttonHAPUS, buttonSEGARKAN, buttonSUNTING, buttonTAMBAH});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void tfPENCARIANKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfPENCARIANKeyTyped
        cfl.Pencarian(tfPENCARIAN.getText().trim());
    }//GEN-LAST:event_tfPENCARIANKeyTyped

    private void buttonTAMBAHActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonTAMBAHActionPerformed
        cfl.insert();
        cfl.kosongkanForm();
        cfl.isiTable();
        cfl.kondisiSemula();
    }//GEN-LAST:event_buttonTAMBAHActionPerformed

    private void buttonSEGARKANActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSEGARKANActionPerformed
        cfl.isiTable();
        cfl.kosongkanForm();
        cfl.kondisiSemula();
    }//GEN-LAST:event_buttonSEGARKANActionPerformed

    private void buttonSUNTINGActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSUNTINGActionPerformed
        cfl.update();
        cfl.kosongkanForm();
        cfl.isiTable();
        cfl.kondisiSemula();
    }//GEN-LAST:event_buttonSUNTINGActionPerformed

    private void buttonHAPUSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonHAPUSActionPerformed
        cfl.delete();
        cfl.kosongkanForm();
        cfl.isiTable();
        cfl.kondisiSemula();
    }//GEN-LAST:event_buttonHAPUSActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonHAPUS;
    private javax.swing.JButton buttonSEGARKAN;
    private javax.swing.JButton buttonSUNTING;
    private javax.swing.JButton buttonTAMBAH;
    private javax.swing.JComboBox cbSTATUS;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tabelFILM;
    private javax.swing.JTextField tfIDFILM;
    private javax.swing.JTextField tfNAMAFILM;
    private javax.swing.JTextField tfPENCARIAN;
    private javax.swing.JTextField tfPRODUKSI;
    private javax.swing.JTextField tfTAHUNRILIS;
    // End of variables declaration//GEN-END:variables
}
