package id.keda87.clickrental.view.sub;

import id.keda87.clickrental.controller.ControllerStaff;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class SubStaff extends javax.swing.JPanel {

    private ControllerStaff cst;

    public SubStaff() {
        initComponents();
        cst = new ControllerStaff(this);
        cst.isiTable();
        cst.kondisiSemula();
        tabel_staff.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                int row = tabel_staff.getSelectedRow();
                if (row != -1) {
                    cst.isiForm(row);
                    cst.kondisiSeleksi();
                }
            }
        });
    }

    public JComboBox getCbJABATAN() {
        return cbJABATAN;
    }

    public JTextArea getTaALAMAT() {
        return taALAMAT;
    }

    public JTextField getTfGAJI() {
        return tfGAJI;
    }

    public JTextField getTfHP() {
        return tfHP;
    }

    public JTextField getTfID() {
        return tfID;
    }

    public JTextField getTfNAMA() {
        return tfNAMA;
    }

    public JTextField getTfPencarian() {
        return tfPencarian;
    }

    public JTable getTabel_staff() {
        return tabel_staff;
    }

    public JButton getButtonDELETE() {
        return buttonDELETE;
    }

    public JButton getButtonINSERT() {
        return buttonINSERT;
    }

    public JButton getButtonRESET() {
        return buttonRESET;
    }

    public JButton getButtonUPDATE() {
        return buttonUPDATE;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabel_staff = new javax.swing.JTable();
        tfID = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        tfPencarian = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        tfNAMA = new javax.swing.JTextField();
        cbJABATAN = new javax.swing.JComboBox();
        jScrollPane2 = new javax.swing.JScrollPane();
        taALAMAT = new javax.swing.JTextArea();
        tfHP = new javax.swing.JTextField();
        tfGAJI = new javax.swing.JTextField();
        buttonINSERT = new javax.swing.JButton();
        buttonRESET = new javax.swing.JButton();
        buttonDELETE = new javax.swing.JButton();
        buttonUPDATE = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        jLabel1.setText("Menu Staff");

        tabel_staff.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tabel_staff);

        jLabel2.setText("ID Staff");

        tfPencarian.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tfPencarianKeyTyped(evt);
            }
        });

        jLabel3.setText("Pencarian");

        jLabel4.setText("Nama");

        jLabel5.setText("Jabatan");

        jLabel6.setText("Alamat");

        jLabel7.setText("No. Handphone");

        jLabel8.setText("Gaji");

        cbJABATAN.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "   ", "KARYAWAN", "ADMIN" }));

        taALAMAT.setColumns(20);
        taALAMAT.setRows(5);
        jScrollPane2.setViewportView(taALAMAT);

        buttonINSERT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/id/keda87/clickrental/icon/add_user-26.png"))); // NOI18N
        buttonINSERT.setText("Tambah");
        buttonINSERT.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonINSERT.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        buttonINSERT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonINSERTActionPerformed(evt);
            }
        });

        buttonRESET.setIcon(new javax.swing.ImageIcon(getClass().getResource("/id/keda87/clickrental/icon/change_user-26.png"))); // NOI18N
        buttonRESET.setText("Segarkan");
        buttonRESET.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonRESET.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        buttonRESET.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonRESETActionPerformed(evt);
            }
        });

        buttonDELETE.setIcon(new javax.swing.ImageIcon(getClass().getResource("/id/keda87/clickrental/icon/remove_user-26.png"))); // NOI18N
        buttonDELETE.setText("Hapus");
        buttonDELETE.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonDELETE.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        buttonDELETE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonDELETEActionPerformed(evt);
            }
        });

        buttonUPDATE.setIcon(new javax.swing.ImageIcon(getClass().getResource("/id/keda87/clickrental/icon/edit_user-26.png"))); // NOI18N
        buttonUPDATE.setText("Sunting");
        buttonUPDATE.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonUPDATE.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        buttonUPDATE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonUPDATEActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 604, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tfPencarian)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(tfID)
                                    .addComponent(tfNAMA)
                                    .addComponent(cbJABATAN, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 236, Short.MAX_VALUE)
                                    .addComponent(tfHP)
                                    .addComponent(tfGAJI)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(buttonUPDATE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(buttonDELETE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(buttonRESET)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(buttonINSERT)))))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {buttonDELETE, buttonINSERT, buttonRESET, buttonUPDATE});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(14, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tfID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(tfNAMA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(cbJABATAN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(tfHP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(tfGAJI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(buttonINSERT)
                            .addComponent(buttonRESET)
                            .addComponent(buttonDELETE)
                            .addComponent(buttonUPDATE)))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 380, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfPencarian, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(15, 15, 15))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {buttonDELETE, buttonINSERT, buttonRESET, buttonUPDATE});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void tfPencarianKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfPencarianKeyTyped
        cst.pencarian(tfPencarian.getText());
    }//GEN-LAST:event_tfPencarianKeyTyped

    private void buttonINSERTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonINSERTActionPerformed
        cst.insert();
        cst.isiTable();
        cst.kosongkanForm();
        cst.kondisiSemula();
    }//GEN-LAST:event_buttonINSERTActionPerformed

    private void buttonRESETActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonRESETActionPerformed
        cst.kosongkanForm();
        cst.kondisiSemula();
        cst.isiTable();
    }//GEN-LAST:event_buttonRESETActionPerformed

    private void buttonDELETEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonDELETEActionPerformed
        cst.delete();
        cst.isiTable();
        cst.kosongkanForm();
        cst.kondisiSemula();
    }//GEN-LAST:event_buttonDELETEActionPerformed

    private void buttonUPDATEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonUPDATEActionPerformed
        cst.update();
        cst.isiTable();
        cst.kosongkanForm();
        cst.kondisiSemula();
    }//GEN-LAST:event_buttonUPDATEActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonDELETE;
    private javax.swing.JButton buttonINSERT;
    private javax.swing.JButton buttonRESET;
    private javax.swing.JButton buttonUPDATE;
    private javax.swing.JComboBox cbJABATAN;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea taALAMAT;
    private javax.swing.JTable tabel_staff;
    private javax.swing.JTextField tfGAJI;
    private javax.swing.JTextField tfHP;
    private javax.swing.JTextField tfID;
    private javax.swing.JTextField tfNAMA;
    private javax.swing.JTextField tfPencarian;
    // End of variables declaration//GEN-END:variables
}
