package id.keda87.clickrental.view.sub;

import id.keda87.clickrental.controller.ControllerMember;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class SubLihatMember extends javax.swing.JPanel {

    private ControllerMember cmm;

    public SubLihatMember() {
        initComponents();
        cmm = new ControllerMember(this);
        cmm.isiTable();
        tableMember.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                int row = tableMember.getSelectedRow();
                if (row != -1) {
                    cmm.isiForm(row);
                }
            }
        });
    }

    public JTable getTableMember() {
        return tableMember;
    }

    public JTextArea getTffAlamat() {
        return tffAlamat;
    }

    public JTextField getTffHandpjone() {
        return tffHandpjone;
    }

    public JTextField getTffNama() {
        return tffNama;
    }

    public JTextField getTffidentitas() {
        return tffidentitas;
    }

    public JTextField getTffPencarian() {
        return tffPencarian;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableMember = new javax.swing.JTable();
        tffPencarian = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        tffidentitas = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        tffNama = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        tffHandpjone = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tffAlamat = new javax.swing.JTextArea();
        buttonEDIT = new javax.swing.JButton();
        buttonREFRESH = new javax.swing.JButton();
        buttonDELETE = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        jLabel1.setText("Menu Lihat Data Member");

        tableMember.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tableMember);

        tffPencarian.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tffPencarianKeyTyped(evt);
            }
        });

        jLabel2.setText("Pencarian");

        jLabel3.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        jLabel3.setText("Sunting Data Member");

        tffidentitas.setEnabled(false);

        jLabel4.setText("Nomor Identitas");

        jLabel5.setText("Nama");

        jLabel6.setText("No. Handphone");

        jLabel7.setText("Alamat");

        tffAlamat.setColumns(20);
        tffAlamat.setLineWrap(true);
        tffAlamat.setRows(5);
        jScrollPane2.setViewportView(tffAlamat);

        buttonEDIT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/id/keda87/clickrental/icon/edit_user-26.png"))); // NOI18N
        buttonEDIT.setText("Sunting");
        buttonEDIT.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonEDIT.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        buttonEDIT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonEDITActionPerformed(evt);
            }
        });

        buttonREFRESH.setIcon(new javax.swing.ImageIcon(getClass().getResource("/id/keda87/clickrental/icon/change_user-26.png"))); // NOI18N
        buttonREFRESH.setText("Segarkan");
        buttonREFRESH.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonREFRESH.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        buttonREFRESH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonREFRESHActionPerformed(evt);
            }
        });

        buttonDELETE.setIcon(new javax.swing.ImageIcon(getClass().getResource("/id/keda87/clickrental/icon/remove_user-26.png"))); // NOI18N
        buttonDELETE.setText("Hapus");
        buttonDELETE.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        buttonDELETE.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        buttonDELETE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonDELETEActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 534, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tffPencarian)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel3))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 42, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel5)
                                            .addComponent(jLabel4)
                                            .addComponent(jLabel6)
                                            .addComponent(jLabel7))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(tffidentitas)
                                            .addComponent(tffNama)
                                            .addComponent(tffHandpjone)
                                            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 361, Short.MAX_VALUE)))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(buttonDELETE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(buttonREFRESH)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(buttonEDIT)))))))
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {buttonDELETE, buttonEDIT, buttonREFRESH});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(27, 27, 27)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tffidentitas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(tffNama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(tffHandpjone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(buttonEDIT)
                            .addComponent(buttonREFRESH)
                            .addComponent(buttonDELETE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tffPencarian, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {buttonDELETE, buttonEDIT, buttonREFRESH});

    }// </editor-fold>//GEN-END:initComponents

    private void buttonREFRESHActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonREFRESHActionPerformed
        cmm.isiTable();
        cmm.kosongkanFormLihat();
    }//GEN-LAST:event_buttonREFRESHActionPerformed

    private void buttonDELETEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonDELETEActionPerformed
        cmm.delete();
        cmm.isiTable();
        cmm.kosongkanFormLihat();
    }//GEN-LAST:event_buttonDELETEActionPerformed

    private void buttonEDITActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonEDITActionPerformed
        cmm.update();
        cmm.isiTable();
        cmm.kosongkanFormLihat();
    }//GEN-LAST:event_buttonEDITActionPerformed

    private void tffPencarianKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tffPencarianKeyTyped
        cmm.Pencarian(tffPencarian.getText().trim());
    }//GEN-LAST:event_tffPencarianKeyTyped
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonDELETE;
    private javax.swing.JButton buttonEDIT;
    private javax.swing.JButton buttonREFRESH;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tableMember;
    private javax.swing.JTextArea tffAlamat;
    private javax.swing.JTextField tffHandpjone;
    private javax.swing.JTextField tffNama;
    private javax.swing.JTextField tffPencarian;
    private javax.swing.JTextField tffidentitas;
    // End of variables declaration//GEN-END:variables
}
