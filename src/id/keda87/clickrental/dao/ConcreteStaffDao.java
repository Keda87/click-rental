package id.keda87.clickrental.dao;

import id.keda87.clickrental.model.Staff;
import id.keda87.clickrental.utilities.ConnectionPool;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ConcreteStaffDao implements StaffDao {

    private ConnectionPool naonweh;

    public ConcreteStaffDao() {
        naonweh = ConnectionPool.getCp();
    }

    @Override
    public void insertStaff(Staff data) throws SQLException {
        naonweh.bukaDB();
        PreparedStatement stat = naonweh.getConn().prepareStatement("insert into staff values(?,?,?,?,?,?)");
        stat.setString(1, data.getId());
        stat.setString(2, data.getNama());
        stat.setString(3, data.getJabatan());
        stat.setString(4, data.getAlamat());
        stat.setString(5, data.getNomorHp());
        stat.setDouble(6, data.getGaji());
        stat.executeUpdate();
    }

    @Override
    public void updataStaff(Staff data) throws SQLException {
        naonweh.bukaDB();
        PreparedStatement stat = naonweh.getConn().prepareStatement("update staff set nama = ?, jabatan = ?, alamat = ?, handphone = ?, gaji = ? where idstaff = ?");
        stat.setString(1, data.getNama());
        stat.setString(2, data.getJabatan());
        stat.setString(3, data.getAlamat());
        stat.setString(4, data.getNomorHp());
        stat.setDouble(5, data.getGaji());
        stat.setString(6, data.getId());
        stat.executeUpdate();
    }

    @Override
    public void deleteStaff(String id) throws SQLException {
        naonweh.bukaDB();
        PreparedStatement stat = naonweh.getConn().prepareStatement("delete from staff where idstaff = ?");
        stat.setString(1, id);
        stat.executeUpdate();
    }

    @Override
    public List<Staff> getAllStaff() throws SQLException {
        naonweh.bukaDB();
        PreparedStatement stat = naonweh.getConn().prepareStatement("select * from staff");
        ResultSet res = stat.executeQuery();
        List<Staff> daftarStaff = new ArrayList<>();
        while (res.next()) {
            Staff staff = new Staff();
            staff.setId(res.getString("idstaff"));
            staff.setNama(res.getString("nama"));
            staff.setJabatan(res.getString("jabatan"));
            staff.setAlamat(res.getString("alamat"));
            staff.setNomorHp(res.getString("handphone"));
            staff.setGaji(res.getDouble("gaji"));
            daftarStaff.add(staff);
        }
        return daftarStaff;
    }

    @Override
    public List<Staff> getSearchStaff(String keyword) throws SQLException {
        naonweh.bukaDB();
        PreparedStatement stat = naonweh.getConn().prepareStatement("select * from staff where idstaff like ? or nama like ? or jabatan like ? or alamat like ? or handphone like ?");
        stat.setString(1, "%" + keyword + "%");
        stat.setString(2, "%" + keyword + "%");
        stat.setString(3, "%" + keyword + "%");
        stat.setString(4, "%" + keyword + "%");
        stat.setString(5, "%" + keyword + "%");
        ResultSet res = stat.executeQuery();
        List<Staff> daftarStaff = new ArrayList<>();
        while (res.next()) {
            Staff staff = new Staff();
            staff.setId(res.getString("idstaff"));
            staff.setNama(res.getString("nama"));
            staff.setJabatan(res.getString("jabatan"));
            staff.setAlamat(res.getString("alamat"));
            staff.setNomorHp(res.getString("handphone"));
            staff.setGaji(res.getDouble("gaji"));
            daftarStaff.add(staff);
        }
        return daftarStaff;
    }
}
