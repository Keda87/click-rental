package id.keda87.clickrental.dao;

import id.keda87.clickrental.model.Nominal;
import java.util.List;

public interface NominalDao {

    public void updateNominal(Nominal price);

    public List<Nominal> getSewaDenda();

    public int getSewa();

    public int getDenda();
}
