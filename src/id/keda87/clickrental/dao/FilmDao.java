package id.keda87.clickrental.dao;

import id.keda87.clickrental.model.Film;
import java.util.List;

public interface FilmDao {

    public void insertFilm(Film film);

    public void updateFilm(Film film);

    public void deleteFilm(int id);

    public List<Film> getAllFilm();

    public List<Film> getSearchFilm(String keyword);
    
    public List<Film> getAllNamaFilm();
    
    public List<Film> getAllKodeFilm(String namaFilm);
    
    public void updateKetersediaan(String judul, String status);
}
