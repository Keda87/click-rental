package id.keda87.clickrental.dao;

import id.keda87.clickrental.model.Transaksi;
import java.util.List;

public interface TransaksiDao {

    public void insertTransaksi(Transaksi data);

    public void updateTransaksi(Transaksi data);

    public void deleteTransaksi(int pk);

    public List<Transaksi> getAllTransaksi();

    public List<Transaksi> getSearchTransaksi(String keyword);
    
    public List<Transaksi> tampilkanGrafik();
}
