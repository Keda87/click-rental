package id.keda87.clickrental.dao;

import id.keda87.clickrental.model.Nominal;
import id.keda87.clickrental.utilities.ConnectionPool;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConcreteNominalDao implements NominalDao {

    private ConnectionPool cp;

    public ConcreteNominalDao() {
        cp = ConnectionPool.getCp();
    }

    @Override
    public void updateNominal(Nominal price) {
        try (PreparedStatement stat = cp.getConn().prepareStatement("update nominal set hargaSewa = ?, dendaSewa = ? where pk = 123")) {
            stat.setInt(1, price.getHargaSewa());
            stat.setInt(2, price.getDendaSewa());
            stat.executeUpdate();
            System.out.println("diperbarui..");
        } catch (SQLException ex) {
            Logger.getLogger(ConcreteNominalDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Nominal> getSewaDenda() {
        List<Nominal> sewadenda = new ArrayList<>();
        try (PreparedStatement stat = cp.getConn().prepareStatement("select * from nominal where pk = 123")) {
            ResultSet res = stat.executeQuery();
            if (res.next()) {
                Nominal sd = new Nominal();
                sd.setHargaSewa(res.getInt("hargaSewa"));
                sd.setDendaSewa(res.getInt("dendaSewa"));
                sewadenda.add(sd);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConcreteNominalDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sewadenda;
    }

    @Override
    public int getSewa() {
        int tmpsewa = 0;
        try (PreparedStatement stat = cp.getConn().prepareStatement("select hargaSewa from nominal where pk = 123")) {
            ResultSet res = stat.executeQuery();
            if(res.next()){
                tmpsewa = res.getInt("hargaSewa");
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConcreteNominalDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tmpsewa;
    }

    @Override
    public int getDenda() {
        int tmpdenda = 0;
        try (PreparedStatement stat = cp.getConn().prepareStatement("select dendaSewa from nominal where pk = 123")) {
            ResultSet res = stat.executeQuery();
            if (res.next()) {
                tmpdenda = res.getInt("dendaSewa");
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConcreteNominalDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tmpdenda;
    }
}