package id.keda87.clickrental.dao;

import id.keda87.clickrental.model.Transaksi;
import id.keda87.clickrental.utilities.ConnectionPool;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConcreteTransaksiDao implements TransaksiDao {

    private ConnectionPool db;

    public ConcreteTransaksiDao() {
        db = ConnectionPool.getCp();
    }

    @Override
    public void insertTransaksi(Transaksi data) {
        try (PreparedStatement stat = db.getConn().prepareStatement("insert into transaksi(idmember, nama, handphone, alamat, namafilm, idfilm, tanggalpinjam, tanggalkembali, totaltransaksi, terlambat, denda) values(?,?,?,?,?,?,?,?,?,?,?)")) {
            stat.setString(1, data.getIdMember());
            stat.setString(2, data.getNama());
            stat.setString(3, data.getHandphone());
            stat.setString(4, data.getAlamat());
            stat.setString(5, data.getNamaFilm());
            stat.setInt(6, data.getIdFilm());
            stat.setString(7, data.getTanggalPinjam());
            stat.setString(8, data.getTanggalKembali());
            stat.setInt(9, data.getTotalTransaksi());
            stat.setInt(10, data.getTerlambat());
            stat.setInt(11, data.getDenda());
            stat.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ConcreteTransaksiDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void updateTransaksi(Transaksi data) {
        try (PreparedStatement stat = db.getConn().prepareStatement("update transaksi set terlambat = ?, denda = ? where idtransaksi = ?")) {
            stat.setInt(1, data.getTerlambat());
            stat.setInt(2, data.getDenda());
            stat.setInt(3, data.getIdTransaksi());
            stat.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ConcreteTransaksiDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void deleteTransaksi(int pk) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Transaksi> getAllTransaksi() {
        List<Transaksi> transaksi = new ArrayList<>();
        try (PreparedStatement stat = db.getConn().prepareStatement("select * from transaksi order by idtransaksi")) {
            ResultSet res = stat.executeQuery();
            while (res.next()) {
                Transaksi data = new Transaksi();
                data.setIdTransaksi(res.getInt("idtransaksi"));
                data.setIdMember(res.getString("idmember"));
                data.setNama(res.getString("nama"));
                data.setHandphone(res.getString("handphone"));
                data.setAlamat(res.getString("alamat"));
                data.setNamaFilm(res.getString("namafilm"));
                data.setIdFilm(res.getInt("idfilm"));
                data.setTanggalPinjam(res.getString("tanggalpinjam"));
                data.setTanggalKembali(res.getString("tanggalkembali"));
                data.setTotalTransaksi(res.getInt("totaltransaksi"));
                data.setTerlambat(res.getInt("terlambat"));
                data.setDenda(res.getInt("denda"));
                transaksi.add(data);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConcreteTransaksiDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return transaksi;
    }

    @Override
    public List<Transaksi> getSearchTransaksi(String keyword) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Transaksi> tampilkanGrafik() {
        List<Transaksi> data = new ArrayList<>();
        try (PreparedStatement stat = db.getConn().prepareStatement("select MONTHNAME(tanggalpinjam) as month, count(*) as jumlah from transaksi group by(month)")) {
            ResultSet res = stat.executeQuery();
            while (res.next()) {
                Transaksi trans = new Transaksi();
                trans.setTanggalPinjam(res.getString("month"));
                trans.setTotalTransaksi(res.getInt("jumlah"));
                data.add(trans);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConcreteTransaksiDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return data;
    }
}
