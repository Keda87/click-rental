package id.keda87.clickrental.dao;

import id.keda87.clickrental.model.Staff;
import java.sql.SQLException;
import java.util.List;

public interface StaffDao {

    public void insertStaff(Staff data) throws SQLException;

    public void updataStaff(Staff data) throws SQLException;

    public void deleteStaff(String id) throws SQLException;

    public List<Staff> getAllStaff() throws SQLException;

    public List<Staff> getSearchStaff(String keyword) throws SQLException;
}
