package id.keda87.clickrental.dao;

import id.keda87.clickrental.model.Member;
import id.keda87.clickrental.utilities.ConnectionPool;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConcreteMemberDao implements MemberDao {

    private ConnectionPool cp;

    public ConcreteMemberDao() {
        cp = ConnectionPool.getCp();
    }

    @Override
    public void insertMember(Member member) {
        PreparedStatement stat = null;
        try {
            stat = cp.getConn().prepareStatement("insert into member values(?,?,?,?)");
            stat.setString(1, member.getId());
            stat.setString(2, member.getNama());
            stat.setString(3, member.getNomorhandphone());
            stat.setString(4, member.getAlamat());
            stat.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ConcreteMemberDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                stat.close();
            } catch (SQLException ex) {
                Logger.getLogger(ConcreteMemberDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void updateMember(Member member) {
        //coba try with resource
        try (PreparedStatement stat = cp.getConn().prepareStatement("update member set nama = ?, handphone = ?, alamat = ? where id = ?")) {
            stat.setString(1, member.getNama());
            stat.setString(2, member.getNomorhandphone());
            stat.setString(3, member.getAlamat());
            stat.setString(4, member.getId());
            stat.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ConcreteMemberDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void deleteMember(String ktp) {
        try (PreparedStatement stat = cp.getConn().prepareStatement("delete from member where id = ?")) {
            stat.setString(1, ktp);
            stat.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ConcreteMemberDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List getAllMember() {
        List daftarMember = new ArrayList();
        try (PreparedStatement stat = cp.getConn().prepareStatement("select * from member")) {
            ResultSet res = stat.executeQuery();
            while (res.next()) {
                Member member = new Member();
                member.setId(res.getString("id"));
                member.setNama(res.getString("nama"));
                member.setNomorhandphone(res.getString("handphone"));
                member.setAlamat(res.getString("alamat"));
                daftarMember.add(member);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConcreteMemberDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return daftarMember;
    }

    @Override
    public List getSearchMember(String keyword) {
        List daftarMember = new ArrayList();
        try (PreparedStatement stat = cp.getConn().prepareStatement("select * from member where id like ? or nama like ? or handphone like ? or alamat like ?")) {
            stat.setString(1, "%" + keyword + "%");
            stat.setString(2, "%" + keyword + "%");
            stat.setString(3, "%" + keyword + "%");
            stat.setString(4, "%" + keyword + "%");
            ResultSet res = stat.executeQuery();
            while (res.next()) {
                Member member = new Member();
                member.setId(res.getString("id"));
                member.setNama(res.getString("nama"));
                member.setNomorhandphone(res.getString("handphone"));
                member.setAlamat(res.getString("alamat"));
                daftarMember.add(member);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConcreteMemberDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return daftarMember;
    }

    @Override
    public List<Member> getAllIDMember() {
        List<Member> idMember = new ArrayList<>();
        try(PreparedStatement stat = cp.getConn().prepareStatement("select id from member")){
            ResultSet res = stat.executeQuery();
            while(res.next()){
                Member id = new Member();
                id.setId(res.getString("id"));
                idMember.add(id);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConcreteMemberDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return idMember;
    }

    @Override
    public List<Member> getIdentitasMember(String id) {
        List<Member> identitas = new ArrayList<>();
        try(PreparedStatement stat = cp.getConn().prepareStatement("select nama, handphone, alamat from member where id = ?")){
            stat.setString(1, id);
            ResultSet res = stat.executeQuery();
            if(res.next()){
                Member identitasLengkap = new Member();
                identitasLengkap.setNama(res.getString("nama"));
                identitasLengkap.setNomorhandphone(res.getString("handphone"));
                identitasLengkap.setAlamat(res.getString("alamat"));
                identitas.add(identitasLengkap);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConcreteMemberDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return identitas;
    }
}
