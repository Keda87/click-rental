package id.keda87.clickrental.dao;

import id.keda87.clickrental.model.Film;
import id.keda87.clickrental.utilities.ConnectionPool;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConcreteFilmDao implements FilmDao {

    private ConnectionPool cp;

    public ConcreteFilmDao() {
        cp = ConnectionPool.getCp();
    }

    @Override
    public void insertFilm(Film film) {
        try (PreparedStatement stat = cp.getConn().prepareStatement("insert into film (namafilm, tahunterbit, produksi, statuskembali) values(?, ?, ?, ?)")) {
            stat.setString(1, film.getNamaFilm());
            stat.setString(2, film.getTahunTerbit());
            stat.setString(3, film.getProduksi());
            stat.setString(4, film.getStatusKembali());
            stat.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ConcreteFilmDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void updateFilm(Film film) {
        try (PreparedStatement stat = cp.getConn().prepareStatement("update film set namafilm = ?, tahunterbit = ?, produksi = ?, statuskembali = ? where id = ?")) {
            stat.setString(1, film.getNamaFilm());
            stat.setString(2, film.getTahunTerbit());
            stat.setString(3, film.getProduksi());
            stat.setString(4, film.getStatusKembali());
            stat.setInt(5, film.getId());
            stat.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ConcreteFilmDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void deleteFilm(int id) {
        try (PreparedStatement stat = cp.getConn().prepareStatement("delete from film where id = ?")) {
            stat.setInt(1, id);
            stat.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ConcreteFilmDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Film> getAllFilm() {
        List<Film> list = new ArrayList<>();
        try (PreparedStatement stat = cp.getConn().prepareStatement("select * from film order by id")) {
            ResultSet res = stat.executeQuery();
            while (res.next()) {
                Film film = new Film();
                film.setId(res.getInt("id"));
                film.setNamaFilm(res.getString("namafilm"));
                film.setTahunTerbit(res.getString("tahunterbit"));
                film.setProduksi(res.getString("produksi"));
                film.setStatusKembali(res.getString("statuskembali"));
                list.add(film);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConcreteFilmDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public List<Film> getSearchFilm(String keyword) {
        List<Film> list = new ArrayList<>();
        try (PreparedStatement stat = cp.getConn().prepareStatement("select * from film where id like ? or namafilm like ? or tahunterbit like ? or produksi like ? or statuskembali like ?")) {
            stat.setString(1, "%" + keyword + "%");
            stat.setString(2, "%" + keyword + "%");
            stat.setString(3, "%" + keyword + "%");
            stat.setString(4, "%" + keyword + "%");
            stat.setString(5, "%" + keyword + "%");
            ResultSet res = stat.executeQuery();
            while (res.next()) {
                Film film = new Film();
                film.setId(res.getInt("id"));
                film.setNamaFilm(res.getString("namafilm"));
                film.setTahunTerbit(res.getString("tahunterbit"));
                film.setProduksi(res.getString("produksi"));
                film.setStatusKembali(res.getString("statuskembali"));
                list.add(film);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConcreteFilmDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public List<Film> getAllNamaFilm() {
        List<Film> namafilm = new ArrayList<>();
        try (PreparedStatement stat = cp.getConn().prepareStatement("select namafilm from film where statuskembali = 'Tersedia'")) {
            ResultSet res = stat.executeQuery();
            while (res.next()) {
                Film namaFilm = new Film();
                namaFilm.setNamaFilm(res.getString("namafilm"));
                namafilm.add(namaFilm);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConcreteFilmDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return namafilm;
    }

    @Override
    public List<Film> getAllKodeFilm(String namaFilm) {
        List<Film> kodefilm = new ArrayList<>();
        try (PreparedStatement stat = cp.getConn().prepareStatement("select id from film where namafilm = ?")) {
            stat.setString(1, namaFilm);
            ResultSet res = stat.executeQuery();
            if (res.next()) {
                Film kodeFilm = new Film();
                kodeFilm.setId(res.getInt("id"));
                kodefilm.add(kodeFilm);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConcreteFilmDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return kodefilm;
    }

    @Override
    public void updateKetersediaan(String judul, String status) {
        try (PreparedStatement stat = cp.getConn().prepareStatement("update film set statuskembali = ? where namafilm = ?")) {
            stat.setString(1, status);
            stat.setString(2, judul);
            stat.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ConcreteFilmDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
