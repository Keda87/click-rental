package id.keda87.clickrental.dao;

import id.keda87.clickrental.model.Member;
import java.util.List;

public interface MemberDao {

    public void insertMember(Member member);

    public void updateMember(Member member);

    public void deleteMember(String ktp);

    public List getAllMember();

    public List getSearchMember(String keyword);
    
    public List<Member> getAllIDMember();
    
    public List<Member> getIdentitasMember(String id);
}
