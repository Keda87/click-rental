package id.keda87.clickrental.utilities;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionPool {

    private static ConnectionPool cp;
    private Connection conn = null;

    private ConnectionPool() {
    }

    public static ConnectionPool getCp() {
        synchronized (ConnectionPool.class) {
            if (cp == null) {
                cp = new ConnectionPool();
            }
        }
        return cp;
    }
    
    public void bukaDB(){
        boolean flag = true;
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            System.out.println(e.getMessage());
            flag = false;
        }
        
        if(flag){
            try {
                conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/clickrental", "root", "");
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }
    
    public Connection getConn(){
        return conn;
    }
}
